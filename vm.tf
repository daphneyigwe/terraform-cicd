
terraform {
  required_providers {
    local = {
      source = "hashicorp/local"
      version = "2.1.0"
    }
  }
}

resource "local_file" "foo" {
    content     = "foo!"
    filename = "foo.txt"
}
